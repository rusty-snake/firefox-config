#!/usr/bin/env -S scala-cli shebang

//> using toolkit default
//> using option -new-syntax -explain -deprecation -feature -unchecked -Wunused:all

case class SearchEngineIcon(uri: String, mime: String, size: Int)
case class SearchEngine(
    shortName: String,
    description: String,
    icon: SearchEngineIcon,
    url: String,
    param: String,
    fileName: String
)

val PluginsDirectory = os
  .followLink(os.Path(scriptPath, base = os.pwd) / os.up)
  .get / os.up / "plugins"

val n = "\n"
val IndexHtmlHead = s"""<!DOCTYPE html><html><head><meta charset="utf-8" />$n"""
val IndexHtmlLinkTemplate =
  s"""<link rel="search" type="application/opensearchdescription+xml" title="%s" href="plugins/%s" />$n"""
val IndexHtmlTail = s"""</head></html>$n"""

val OpenSearchTemplate = """<?xml version="1.0" encoding="UTF-8"?>
<OpenSearchDescription xmlns="http://a9.com/-/spec/opensearch/1.1/">
  <ShortName>%s</ShortName>
  <Description>%s</Description>
  <InputEncoding>UTF-8</InputEncoding>
  <Image width="%s" height="%s" type="%s">%s</Image>
  <Url type="text/html" method="get" template="%s">
    <Param name="%s" value="{searchTerms}"/>
  </Url>
</OpenSearchDescription>
"""

val SearchEngines = List(
  SearchEngine(
    shortName = "DuckDuckGo JS",
    description = "Privacy, simplified.",
    icon = SearchEngineIcon(
      uri = "https://duckduckgo.com/favicon.ico",
      mime = "image/x-icon",
      size = 32
    ),
    url = "https://duckduckgo.com/",
    param = "q",
    fileName = "duckduckgo.xml"
  ),
  SearchEngine(
    shortName = "DuckDuckGo HTML",
    description = "Privacy, simplified.",
    icon = SearchEngineIcon(
      uri = "https://duckduckgo.com/favicon.ico",
      mime = "image/x-icon",
      size = 32
    ),
    url = "https://html.duckduckgo.com/html",
    param = "q",
    fileName = "duckduckgo_html.xml"
  ),
  SearchEngine(
    shortName = "DuckDuckGo Lite",
    description = "Privacy, simplified.",
    icon = SearchEngineIcon(
      uri = "https://duckduckgo.com/favicon.ico",
      mime = "image/x-icon",
      size = 32
    ),
    url = "https://lite.duckduckgo.com/lite",
    param = "q",
    fileName = "duckduckgo_lite.xml"
  ),
  SearchEngine(
    shortName = "Ecosia",
    description = "The search engine that plants trees",
    icon = SearchEngineIcon(
      uri = "https://cdn-static.ecosia.org/assets/images/ico/favicon.ico",
      mime = "image/x-icon",
      size = 64
    ),
    url = "https://www.ecosia.org/search",
    param = "q",
    fileName = "ecosia.xml"
  ),
  SearchEngine(
    shortName = "MetaGer",
    description = "Sicher suchen &amp; finden",
    icon = SearchEngineIcon(
      uri = "https://metager.de/favicon.ico",
      mime = "image/x-icon",
      size = 64
    ),
    url = "https://metager.de/meta/meta.ger3",
    param = "eingabe",
    fileName = "metager_de.xml"
  ),
  SearchEngine(
    shortName = "MetaGer (en)",
    description = "Privacy Protected Search &amp; Find",
    icon = SearchEngineIcon(
      uri = "https://metager.org/favicon.ico",
      mime = "image/x-icon",
      size = 64
    ),
    url = "https://metager.org/meta/meta.ger3",
    param = "eingabe",
    fileName = "metager_en.xml"
  ),
  SearchEngine(
    shortName = "Mojeek",
    description =
      "The alternative search engine that puts the people who use it first",
    icon = SearchEngineIcon(
      uri = "https://www.mojeek.com/favicon.png",
      mime = "image/png",
      size = 32
    ),
    url = "https://www.mojeek.com/search",
    param = "q",
    fileName = "mojeek.xml"
  ),
  SearchEngine(
    shortName = "Qwant",
    description = "The search engine that respects your privacy.",
    icon = SearchEngineIcon(
      uri = "https://www.qwant.com/favicon.ico",
      mime = "image/x-icon",
      size = 16
    ),
    url = "https://www.qwant.com/",
    param = "q",
    fileName = "qwant.xml"
  ),
  SearchEngine(
    shortName = "Qwant Lite",
    description = "The search engine that respects your privacy.",
    icon = SearchEngineIcon(
      uri = "https://www.qwant.com/favicon.ico",
      mime = "image/x-icon",
      size = 16
    ),
    url = "https://lite.qwant.com/",
    param = "q",
    fileName = "qwant_lite.xml"
  ),
  SearchEngine(
    shortName = "Startpage",
    description = "Private Search Engine",
    icon = SearchEngineIcon(
      uri =
        "https://www.startpage.com/sp/cdn/favicons/favicon-16x16--default.png",
      mime = "image/png",
      size = 16
    ),
    url = "https://www.startpage.com/sp/search",
    param = "query",
    fileName = "startpage.xml"
  ),
  SearchEngine(
    shortName = "Brave Search",
    description = "Private Search Engine",
    icon = SearchEngineIcon(
      uri =
        "https://cdn.search.brave.com/serp/v2/_app/immutable/assets/favicon-32x32.B2iBzfXZ.png",
      mime = "image/png",
      size = 32
    ),
    url = "https://search.brave.com/search",
    param = "q",
    fileName = "brave.xml"
  )
)

val indexHtml = StringBuilder(IndexHtmlHead)
for searchEngine <- SearchEngines do
  os.write.over(
    PluginsDirectory / searchEngine.fileName,
    OpenSearchTemplate.format(
      searchEngine.shortName,
      searchEngine.description,
      searchEngine.icon.size,
      searchEngine.icon.size,
      searchEngine.icon.mime,
      searchEngine.icon.uri,
      searchEngine.url,
      searchEngine.param
    )
  )
  indexHtml.append(
    IndexHtmlLinkTemplate.format(searchEngine.shortName, searchEngine.fileName)
  )
indexHtml.append(IndexHtmlTail)
os.write.over(PluginsDirectory / os.up / "index.html", indexHtml.result())
