Firefox Config
==============

My config for a hardened, secure, privacy friendly firefox.

Content
-------

- uBlock Origin
  - Enable "Advanced user mode"
  - Allow it in PBM
  - Enable additional filter lists:
    - Block Outsider Intrusion into LAN
    - AdGuard URL Tracking Protection
    - AdGuard Tracking Protection
  - Update My filters with `assets/uBlockOrigin:My_filters.txt`
  - Update My rules with `assets/uBlockOrigin:My_rules.txt`
- user-overrides.js
  - `assets/user-overrides.js`
  - `assets/updater2.sh`
- `search-plugins`

License: CC0
------------

[COPYING](COPYING)

```
Firefox Config - Config for a hardened, secure, privacy friendly firefox.

Written in 2020-2024 by rusty-snake

To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
```
