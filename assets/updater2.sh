#!/bin/bash

# Firefox Config - Config for a hardened, secure, privacy friendly firefox.
#
# Written in 2021 by rusty-snake
#
# To the extent possible under law, the author(s) have dedicated all copyright and
# related and neighboring rights to this software to the public domain worldwide.
# This software is distributed without any warranty.
#
# You should have received a copy of the CC0 Public Domain Dedication along with
# this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.

set -euo pipefail

trap "echo 'An error occurred'" EXIT

cd -P -- "$(readlink -e "$(dirname "$0")")"

# Create a backup of the current user.js if it exists
if [[ -e user.js ]]; then
	mkdir -p userjs_backups
	cp user.js userjs_backups/"$(date --utc --iso-8601=seconds)"_user.js
fi

ARKENFOX_USERJS="https://raw.githubusercontent.com/arkenfox/user.js/master/user.js"
wget2 --https-enforce=hard --secure-protocol=TLSv1_3 -qO- "$ARKENFOX_USERJS" > user.js

cat user-overrides.js >> user.js

echo "Success"

trap - EXIT
exit 0
