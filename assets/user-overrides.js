/*** OVERRIDES ***/
/*** ========= ***/

/* DNS-over-HTTPS (DoH)
 * https://www.privacyguides.org/en/dns/#recommended-providers */
user_pref("network.trr.mode", 3);
user_pref("network.trr.uri", "https://unfiltered.adguard-dns.com/dns-query");
user_pref("network.trr.custom_uri", "https://unfiltered.adguard-dns.com/dns-query"); // Cosmetic
user_pref("network.trr.bootstrapAddr", "94.140.14.140"); // [HIDDEN PREF]

/* Session restore without history */
user_pref("browser.startup.page", 3);
user_pref("privacy.clearOnShutdown.history", false);
user_pref("privacy.clearOnShutdown_v2.historyFormDataAndDownloads", false);
user_pref("browser.sessionstore.privacy_level", 0);
user_pref("places.history.enabled", false);

/* Disable cross-origin referer. [PRIVACY] */
user_pref("network.http.referer.XOriginPolicy", 2);

/* Hide private IPs. */
user_pref("media.peerconnection.ice.no_host", true);
// user_pref("media.peerconnection.ice.relay_only", true);

user_pref("browser.link.force_default_user_context_id_for_external_opens", true);

/* Enable HoM for local resources. */
user_pref("dom.security.https_only_mode.upgrade_local", true);

/* Disable OCSP. */
user_pref("security.OCSP.enabled", 0);

// // Tor Browser Security Slider prefs
// // Safer:
// user_pref("gfx.font_rendering.graphite.enabled", false);
// user_pref("gfx.font_rendering.opentype_svg.enabled", false);
// user_pref("javascript.options.asmjs", false);
// user_pref("javascript.options.baselinejit", false);
// user_pref("javascript.options.ion", false);
// user_pref("javascript.options.native_regexp", false);
// user_pref("javascript.options.wasm", false);
// user_pref("mathml.disabled", true);
// // Safest:
// user_pref("svg.disabled", true);

/* jitless-fox [SECURITY]
 *
 * Firefox/SpiderMonkey has four optimization levels for JavaScript:
 *  1. Interpreter
 *  2. Baseline Interpreter (hybrid interpreter/JIT)
 *  3. Baseline Compiler
 *  4. WarpMonkey (former IonMonkey)
 * each depending on the previous ones. Hence if you disable one of them, you should
 * also disable all higher JITs.
 * https://firefox-source-docs.mozilla.org/js/index.html#javascript-jits
 * https://hacks.mozilla.org/2019/08/the-baseline-interpreter-a-faster-js-interpreter-in-firefox-70/
 *
 * For WASM, Firefox/SpiderMonkey has two optimization levels:
 *  1. WASM-Baseline (RabaldrMonkey)
 *  2. WASM-Ion (BaldrMonkey)
 */
user_pref("javascript.options.ion", false); // WarpMonkey
user_pref("javascript.options.baselinejit", false); // Baseline Compiler
// user_pref("javascript.options.blinterp", false); // Baseline Interpreter
user_pref("javascript.options.native_regexp", false); // irregexp JIT
user_pref("javascript.options.wasm_optimizingjit", false); // WASM-Ion
// user_pref("javascript.options.wasm_baselinejit", false); // WASM-Baseline
user_pref("javascript.options.jit_trustedprincipals", true);
/* Disable asm.js
 * asm.js is the older,little brother of wasm.
 * disabling it shouldn't cause any non-performance issues. */
user_pref("javascript.options.asmjs", false);
/* Disable wasm
 * Firefox needs per-site WASM permission!! */
// user_pref("javascript.options.wasm", false);

/* W^X for JIT pages */
user_pref("javascript.options.content_process_write_protect_code", true);

/* Do not disable spectre mitigations for isolated content [TB] [SECURITY] */
user_pref("javascript.options.spectre.disable_for_isolated_content", false);

/* Add additional RFPTargets to FPP
 *
 * If one of them causes breakage, allow it for specific sites with the
 * `privacy.fingerprintingProtection.granularOverrides' pref.
 *
 * All RFPTargets:
 * https://searchfox.org/mozilla-central/source/toolkit/components/resistfingerprinting/RFPTargets.inc
 * RFPTargets that are used by default:
 * https://searchfox.org/mozilla-central/source/toolkit/components/resistfingerprinting/RFPTargetsDefault.inc
 *
 * +ReduceTimerPrecision
 * ---------------------
 *
 * Drastically reduces the timer precision. This is not only a privacy (Keystroke dynamics)
 * but also a security feature because a lot of (site-channel) exploits need accurate high precision timers.
 *
 * The default timer precision is 100ms, however in cross-origin isolated contexts it is increased to 5ms.
 * https://developer.mozilla.org/en-US/docs/Web/API/Performance/now
 *
 * SharedArrayBuffers can be used to create high-resolution timers. Therefore we disable sharing them
 * in cross-origin isolated contexts (in unisolated contexts shareing is already disabled).
 *
 * To estimate the timer precision you can use this javascript snippet in the console:
 * (() => { const start = performance.now(); let diff; while ((diff = performance.now() - start) === 0); return diff; })();
 * source: https://stackoverflow.com/questions/50117537/how-to-get-microsecond-timings-in-javascript-since-spectre-and-meltdown
 *
 * To get a cross-origin isolated context for testing:
 * caddy respond -H 'Cross-Origin-Opener-Policy: same-origin' -H 'Cross-Origin-Embedder-Policy: require-corp' -l 127.0.0.1:8080
 *
 * +WebGLRenderCapability,+WebGLRenderInfo
 * ---------------------------------------
 *
 * Reduce WebGL fingerprint.
 *
 * +UseStandinsForNativeColors
 * ---------------------------
 *
 * Prevent system colors from being exposed to CSS or canvas.
 */
// TODO: "+AllTargets,-JSDateTimeUTC,-HttpUserAgent" (-CanvasImageExtractionPrompt?)
user_pref("privacy.fingerprintingProtection.overrides", "+ReduceTimerPrecision,+WebGLRenderCapability,+WebGLRenderInfo,+UseStandinsForNativeColors");
user_pref("dom.postMessage.sharedArrayBuffer.withCOOP_COEP", false);
user_pref("privacy.resistFingerprinting.letterboxing", false); // To find the pref in about:config

/* Granular FPP overrides / FPP Exceptions
 *
 * - firstPartyDomain: apple.com
 *   overrides: -WebGLRenderCapability
 *   reason: Breaks "Look Around" on beta.maps.apple.com
 */
user_pref("privacy.fingerprintingProtection.granularOverrides", "[{\"firstPartyDomain\": \"apple.com\", \"overrides\": \"-WebGLRenderCapability\"}]");

/* Disable Fingerprinting Overrides from RemoteSettings
 * Jan 2025: The only overrides is to disable CanvasRandomization for `google.*' sites. */
user_pref("privacy.fingerprintingProtection.remoteOverrides.enabled", false);

/* Disable WebGL; Firefox needs click2play/per-site WebGL permission!! */
// user_pref("webgl.disabled", true);

/** PERSONAL **/

/* Enable cookie banner handling in "Reject-all if possible, otherwise do nothing." mode in all windows. */
user_pref("cookiebanners.service.mode", 1);

/* Reset to firefox defaults */
user_pref("browser.download.always_ask_before_handling_new_types", false); // User interaction is already enforced for all downloads via browser.download.useDownloadDir
user_pref("network.negotiate-auth.trusted-uris", ""); // reset to Mozilla Firefox default, changed by RedHat/Fedora

/* Do not show open-with dialog. */
user_pref("browser.download.forbid_open_with", true);

/* Whitelist additionals ports (to find the pref on about:config and reset it on restart). */
user_pref("network.security.ports.banned.override", "");

/* Annoyances */
user_pref("browser.preferences.moreFromMozilla", false);
user_pref("browser.translations.automaticallyPopup", false);
user_pref("datareporting.policy.dataSubmissionPolicyBypassNotification", true);
user_pref("extensions.pocket.enabled", false);
user_pref("identity.fxaccounts.enabled", false);

/* Disable saving passwords; KeePassXC */
user_pref("signon.rememberSignons", false);

/* Personal behaviour preference; Interfers with RFPTarget SiteSpecificZoom! */
user_pref("browser.zoom.siteSpecific", false);

/* Customize bookmarks:
 * - always show the bookmarks toolbari
 * - save new bookmarks in "Other bookmarks" (old standard behaviour) */
user_pref("browser.toolbars.bookmarks.visibility", "always");
user_pref("browser.bookmarks.defaultLocation", "unfiled");

/* Use "Transient user gesture activation" policy for autoplay blocking.
 * https://wiki.mozilla.org/Media/block-autoplay */
user_pref("media.autoplay.blocking_policy", 1);

/* Do not suggest open tabs in the urlbar. */
user_pref("browser.urlbar.suggest.openpage", false);

/* Enable disk cache */
// user_pref("browser.cache.disk.enable", true);



/* Enable experimental Firefox feature */
user_pref("browser.tabs.groups.enabled", true); // Tab Groups
user_pref("browser.history.collectWireframes", true); // hoverPreview for unloaded tabs
// user_pref("sidebar.revamp", true); // new sidebar experience
// user_pref("sidebar.verticalTabs", true); // vertical tabs
// user_pref("privacy.webrtc.globalMuteToggles", true); // Global WebRTC mute toggle
// user_pref("browser.backup.preferences.ui.enabled", true); // Enable BackupService about:preferences UI
// user_pref("browser.privatebrowsing.felt-privacy-v1", true);
// user_pref("security.certerrors.felt-privacy-v1", true);
// user_pref("network.cookie.CHIPS.enabled", true); // CHIPS
// user_pref("browser.profiles.enabled", true);
// user_pref("network.dns.echconfig.fallback_to_origin_when_all_failed", true);
user_pref("browser.tabs.unloadTabInContextMenu", true);









user_pref("extensions.webcompat.smartblockEmbeds.enabled", true);

user_pref("browser.download.enable_spam_prevention", true); // Enabled by TB

// Would be cool if it would work for cookie permission too.
// user_pref("permissions.isolateBy.userContext", true);

user_pref("privacy.restrict3rdpartystorage.heuristic.opened_window_after_interaction", false);
user_pref("privacy.restrict3rdpartystorage.heuristic.recently_visited", false);
user_pref("privacy.restrict3rdpartystorage.heuristic.window_open", false);
